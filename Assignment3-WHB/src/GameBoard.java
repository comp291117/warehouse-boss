import java.awt.Color;
import java.awt.Graphics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

/**
 * The GameBoard class contains all the information regarding the visuals of the game.
 * That is, the painting and placement of each of the individual components.
 * 
 */

public class GameBoard {
	
	private int[][] map;
	private int noMoves;
	private String mode;
	private int xSize;
	private int ySize;
	private Player p;
	private Player p2;
	private int startingMoves;

	private ArrayList<Floor> Floors;
	private ArrayList<Wall> Walls;
	private ArrayList<Goal> Goals;
	private ArrayList<Box> Boxes;

	private int scale = 0;
	
	/**
	 * The GameBoard holds mainly the backend of the game
	 * 
	 */
	public GameBoard() {
		
		this.mode = GameState.gameMode;
		
		Floors = new ArrayList<Floor>();
		Walls = new ArrayList<Wall>();
		Goals = new ArrayList<Goal>();
		Boxes = new ArrayList<Box>();
		
		this.startingMoves = GameState.moves;

		if (GameState.gameMode.equals("MULTI")) {
			initialiser(20,20);
			loadLevel(new File("src/resources/Map/multi.txt"));
			
			generateNew();
		} else if (GameState.gameMode.equals("NORMAL") || mode.equals("TIMED")) {
			int x = getDimensions("x", new File("src/resources/Map/Map" + GameState.level + ".txt"));
			int y = getDimensions("y", new File("src/resources/Map/Map" + GameState.level + ".txt"));
			initialiser(x,y);
			populateFloors();
			loadLevel(new File("src/resources/Map/Map" + GameState.level + ".txt"));
						
		} else {
			// really dodgy fix atm until we can find size of map from the text
			// file
			initialiser(10+GameState.level,10+GameState.level);
			System.gc();
			generateNew();
		}
		this.noMoves = 0;
		//System.out.println(toString());
	}

	/**
	 * This method generates a new, random map.
	 * 
	 * @pre: A game is running
	 * @param: n/a
	 * @post: A new map is generated
	 * @return: void
	 * 
	 */
	
	public void generateNew() {
		clearMap();

		populateFloors();
		populateWalls();

		generate(2 * this.getxSize(), this.getxSize() / 4);

		boolean colourChange = false;

		for (Box b : this.Boxes) {
			for (Goal g : this.Goals) {
				if (g.hasBox(b)) {
					b.setOnGoal(true);
					colourChange = true;
				}
			}

			if (!colourChange) {
				b.setOnGoal(false);
			}
			break;
		}
		
	}
	
	/**
	 * This method returns player object from a string which indicates if
	 * Player 1 or Player 2 is required
	 * 
	 * @pre players already exist
	 * @param character - the character to be altered (Player 1 or Player 2)
	 * @post the correct player is returned
	 * @return Player - P1 if it's Player1 and P2 if not
	 * 
	 */
	
	public Player checkCharacter(String character) {
		if (p2 == null) {
			return p;
		}
		if (character.equals("P2")) {
			return p2;
		}
		return p;
	}
	
	/**
	 * This method checks and moves a character in the requeted direction
	 * 
	 * @pre the maximum distance is one block. character is valid.
	 * @param direction - the requested movement to be made
	 * @param character -  the character to be changed
	 * @post the character is moved only if the requested move is valid
	 * @return void
	 * 
	 */

	public boolean moveChar(Vector direction, String character) {
		p.Rcounter=0;
		p.Lcounter=0;
		p.Dcounter=0;
		p.Ucounter=0;
		Player toMove = checkCharacter(character);

		boolean moved = false;
		boolean colourChange = false;

		switch (getNextBlock(direction, character)) {
		case "EMPTY":
			toMove.move(direction);
			moved = true;
			break;
		case "WALL":
			break;
		case "GOAL":
			toMove.move(direction);
			moved = true;
			break;
		case "BLOCK":
			if (canPush(direction, character)) {
				Box.playSoundsbox();
				toMove.move(direction);
				for (Box b : this.Boxes) {
					if (b.isThere(toMove.getX(), toMove.getY())) {
						b.move(direction);
						for (Goal g : this.Goals) {
							if (g.hasBox(b)) {
								b.setOnGoal(true);
								colourChange = true;
							}
						}

						if (!colourChange) {
							b.setOnGoal(false);
						}
						moved = true;
						break;
					}
				}
			}
			break;
		case "PLAYER":
			break;
		case "PLAYER2":
			break;
		default:
			break;
		}

		if (moved) {
			toMove.setHasMoved(true);
			GameState.moves++;
			return true;
		}
		return false;
	}
	
	/**
	 * This method identifies the block in the way of the character's
	 * requested direction
	 * 
	 * @pre the maximum distance is one block. character is valid. 
	 * @param direction - the requested movement to be made
	 * @param character - the character to be changed
	 * @post the correct String associated with the block is returned
	 * @return String - the name of the next block
	 * 
	 */

	public String getNextBlock(Vector direction, String character) {

		Player toMove = checkCharacter(character);

		if ((toMove.getX() + direction.getDx()) < 0 || (toMove.getY() + direction.getDy()) < 0) {
			return "ERROR";
		} else if ((toMove.getX() + direction.getDx()) > this.xSize
				|| (toMove.getY() + direction.getDy()) > this.ySize) {
			return "ERROR";
		}

		switch (this.getMap()[toMove.getX() + direction.getDx()][toMove.getY() + direction.getDy()]) {
		case 1:
			return "EMPTY";
		case 2:
			return "WALL";
		case 3:
			return "GOAL";
		case 4:
			return "BLOCK";
		case 5:
			return "PLAYER";
		case 6:
			return "PLAYER2";
		default:
			return "ERROR";
		}

	}
	
	/**
	 * This method determines if a character is able to push a block in the
	 * given direction
	 * 
	 * @pre the maximum distance is one block. character is valid.
	 * @param direction - the requested movement to be made
	 * @param character - the character to be changed
	 * @post returns the correct boolean value
	 * @return boolean - TRUE if the block can be pushed, FALSE if not
	 * 
	 */
	
	public boolean canPush(Vector direction, String character) {

		Player toMove = checkCharacter(character);

		int x = toMove.getX();
		int y = toMove.getY();

		// check if the next move is a block
		if (this.getMap()[x + direction.getDx()][y + direction.getDy()] == 4) {

			// check if the block can be moved
			if ((x + direction.getDx() + direction.getDx()) < 0 || (y + direction.getDy() + direction.getDy()) < 0) {
				return false;
			} else if ((x + direction.getDx() + direction.getDx()) > this.xSize
					|| (y + direction.getDy() + direction.getDy()) > this.ySize) {
				return false;
			}

			// check if next next move is empty
			// i.e for graph 3 4 1 1
			// i.e check if 3 4 and 3 1

			if (((this.getMap()[x + direction.getDx() + direction.getDx()][y + direction.getDy()
					+ direction.getDy()] == 1)
					|| (this.getMap()[x + direction.getDx() + direction.getDx()][y + direction.getDy()
							+ direction.getDy()] == 3)))
				return true;
		}
		return false;
	}

	/**
	 * This method checks whether all the blocks are on the goals
	 * 
	 * @pre there are blocks and goals on the map
	 * @param n/a
	 * @post the correct number of blocks and goals are compared
	 * @return boolean - TRUE if all the blocks are on the goals and FALSE if otherwise
	 * 
	 */
	
	public boolean hasWon() {
				
		int i = 0, j = 0;
		for (Goal g : this.Goals) {
			for (Box b : this.Boxes) {
				if (g.hasBox(b)) {
					i++;
					break;
				}
			}
			j++;
		}
		return (i == j);
	}
	
	/**
	 * This method prints out the maze matrix into a string for the user to see
	 * in the console.
	 * 
	 * @pre maze matrix is available to be printed
	 * @param n/a
	 * @post maze is correctly printed onto the console
	 * @preturn String - a visual matrix representing the game map
	 * 
	 */

	public String toString() {
		String toReturn = "";

		for (int i = 0; i < this.getMap().length; i++) {
			for (int j = 0; j < this.getMap()[i].length; j++) {
				toReturn += this.getMap()[j][i] + " ";
			}
			toReturn += "\n";
		}

		return toReturn;
	}
	
	/**
	 * This method restarts the current level and restores the boxes and characters to
	 * the original position
	 * 
	 * @pre the current level/map is valid
	 * @param n/a
	 * @post the sprites are restored to the original position
	 * @return void
	 * 
	 */

	public void restart() {
		for (Box b : this.Boxes) {
			this.map[b.getX()][b.getY()] = 1;
			b.setX(b.getXinit());
			b.setY(b.getYinit());
			for (Goal g : this.Goals) {
				if (g.hasBox(b)) {
					b.setOnGoal(true);
					break;
				} else
					b.setOnGoal(false);

			}
		}
		this.p.setX(this.p.getXinit());
		this.p.setY(this.p.getYinit());

		if (this.mode.equals("MULTI")) {
			this.p2.setX(this.p2.getXinit());
			this.p2.setY(this.p2.getYinit());
		}
		
		GameState.moves = this.startingMoves;
		
	}
	
	/**
	 * This method loads an arraylist of objects into the matrix.
	 * 
	 * @pre arraylists have been initialised and aren't empty.
	 * @param n/a
	 * @post arraylists are loaded onto a matrix for the map
	 * @return void
	 *  
	 */

	public void LoadMap() {
		for (Floor f : this.Floors) {
			if (f != null) {
				map[f.getX()][f.getY()] = 1;
			}
		}

		for (Wall w : this.Walls) {
			if (w != null) {
				map[w.getX()][w.getY()] = 2;
			}
		}
		for (Goal g : this.Goals) {
			if (g != null) {
				map[g.getX()][g.getY()] = 3;
			}
		}
		for (Box b : this.Boxes) {
			if (b != null) {
				map[b.getX()][b.getY()] = 4;
			}
		}

		if (p != null) {
			this.map[p.getX()][p.getY()] = 5;
		}
		if (this.mode.equals("MULTI") || (p2 != null)) {
			this.map[p2.getX()][p2.getY()] = 6;
		}
	}

	/**
	 * This method paints the visuals of the game
	 * 
	 * @pre arraylists of sprites have been initialised. graphics and gamestate are
	 * valid
	 * @param graphic - the icon to be painted
	 * @param game - the current GameState being played
	 * @post visuals are correctly displayed
	 * @return void
	 * 
	 */
	
	public void PaintMap(Graphics graphic, GameState game) {
		for (Floor f : this.Floors) {
			if (f != null) {
				paint(graphic, game, f);
			}
		}

		for (Wall w : this.Walls) {
			if (w != null) {
				paint(graphic, game, w);
			}
		}

		for (Goal g : this.Goals) {
			if (g != null) {
				paint(graphic, game, g);
			}
		}

		for (Box b : this.Boxes) {
			if (b != null) {
				paint(graphic, game, b);
			}
		}

		if (p != null) {
			paintP(graphic, game, p);
		}

		if ((this.mode.equals("MULTI")) && (p2 != null)) {
			paintP(graphic, game, p2);
		}

		if (mode.equals("TIMED")) {
			graphic.setColor(Color.RED);
			graphic.fillRect(40, 35, 27, 20);
			graphic.setColor(Color.BLACK);
			graphic.drawString(Integer.toString(game.getTimeRemaining()), 50, 50);
		}

	}

	/**
	 * Paints object
	 * 
	 * @pre Player exists and Game exists
	 * @param 	graphic - graphic that we are drawing on
	 * 			game - current GameState
	 * 			objects - to be painted
	 * @post printed out object onto graphic
	 * @return void
	 * 
	 */
	
	public void paint(Graphics graphic, GameState game, SpriteProperties object) {

		graphic.drawImage(object.getIcon(), object.getX() * scale, object.getY() * scale, scale, scale, game);

	}
	
	/**
	 * Paints players sprite
	 * 
	 * @pre Player exists and Game exists
	 * @param 	graphic - graphic that we are drawing on
	 * 			game - current GameState
	 * 			objects - player to be printed
	 * @post printed out player's current sprite
	 * @return void
	 * 
	 */
	
	public void paintP(Graphics graphic, GameState game, Player object) {

		if (object.start == 0)
			graphic.drawImage(object.getIcon(), object.getX() * scale, object.getY() * scale, scale, scale, game);
		else if (object.getDirection().getDx() == -1) {
			graphic.drawImage(object.getPlayerIcon("left"), object.getX() * scale, object.getY() * scale, scale, scale,
					game);
		} else if (object.getDirection().getDx() == 1) {
			graphic.drawImage(object.getPlayerIcon("right"), object.getX() * scale, object.getY() * scale, scale, scale,
					game);
		} else if (object.getDirection().getDy() == -1) {
			graphic.drawImage(object.getPlayerIcon("up"), object.getX() * scale, object.getY() * scale, scale, scale,
					game);
		} else if (object.getDirection().getDy() == 1) {
			graphic.drawImage(object.getPlayerIcon("down"), object.getX() * scale, object.getY() * scale, scale, scale,
					game);
		} else {
			graphic.drawImage(object.getPlayerIcon("right"), object.getX() * scale, object.getY() * scale, scale, scale,
					game);
		}
	}

	/**
	 * This method populates the matrix with floor objects because there is a floor
	 * underneath it all.
	 * 
	 * @pre matrix is valid
	 * @param n/a
	 * @post whole matrix has floor objects
	 * @return void
	 * 
	 */

	public void populateFloors() {
		for (int i = 0; i < this.getMap().length; i++) {
			for (int j = 0; j < this.getMap()[i].length; j++) {
				Floors.add(new Floor(i, j));
			}
		}
	}

	/**
	 * This method populates the matrix with walls
	 * 
	 * @pre matrix is valid
	 * @param n/a
	 * @post
	 * @return void
	 */
	
	public void populateWalls() {
		for (int i = 0; i < this.getMap().length; i++) {
			for (int j = 0; j < this.getMap()[i].length; j++) {
				Walls.add(new Wall(i, j));
			}
		}
	}

	/**
	 * This generates a map
	 * 
	 * @pre map has been initialized
	 * @param	size - number of templates to replace board
	 * 			boxes - number of boxes/goals to be placed
	 * @post level is generated
	 * @return void
	 * 
	 */
	
	public void generate(int size, int boxes) {
		File file = new File("src/resources/Map/temp1.txt");

		for (int i = 0; i < size; i++) {
			int x = random(1, this.getxSize() - 4);
			int y = random(1, this.getxSize() - 4);
			// rand.nextInt((max - min) + 1) + min;
			int block = random(1, 5);
			if (block == 1) {
				file = new File("src/resources/Map/temp1.txt");
			} else if (block == 2) {
				file = new File("src/resources/Map/temp2.txt");
			} else if (block == 3) {
				file = new File("src/resources/Map/temp3.txt");
			} else if (block == 4) {
				file = new File("src/resources/Map/temp4.txt");
			} else if (block == 5) {
				file = new File("src/resources/Map/temp5.txt");
			}
			addMap(x, y, file);
		}

		for (int i = 0; i < boxes; i++) {
			int xBox = random(1, this.getxSize() - 4);
			int yBox = random(1, this.getxSize() - 4);

			int xGoal = random(1, this.getxSize() - 4);
			int yGoal = random(1, this.getxSize() - 4);

			ArrayList<SpriteProperties> objects = getAllObjects();

			while (containsO(objects, new Goal(xGoal, yGoal))) {
				xGoal = random(1, this.getxSize() - 4);
				yGoal = random(1, this.getxSize() - 4);
			}
			objects.add(new Goal(xGoal, yGoal));
			int counter = 0;
			while (containsO(objects, new Box(xBox, yBox))
					// 1 1 1
					// W B 1
					// 1 W 1
					|| ((map[xBox - 1][yBox] == 2))

					// 1 W 1
					// W B 1
					// 1 1 1
					|| ((map[xBox + 1][yBox] == 2))

					// 1 W 1
					// 1 B W
					// 1 1 1
					|| ((map[xBox][yBox + 1] == 2))

					// 1 1 1
					// 1 B W
					// 1 W 1
					|| ((map[xBox][yBox - 1] == 2))

			) {
				counter++;
				xBox = random(1, this.getxSize() - 4);
				yBox = random(1, this.getxSize() - 4);
				if (counter == 1000) {
					generateNew();
					return;
				}
			}
			Boxes.add(new Box(xBox, yBox));
			Goals.add(new Goal(xGoal, yGoal));
			this.LoadMap();
		}

		int xPlayer = random(1, this.getxSize() - 2);
		int yPlayer = random(1, this.getxSize() - 2);

		ArrayList<SpriteProperties> objects = getAllObjects();

		while (containsO(objects, new Player(xPlayer, yPlayer,1))) {
			xPlayer = random(1, this.getxSize() - 2);
			yPlayer = random(1, this.getxSize() - 2);
		}

		p = new Player(xPlayer, yPlayer,1);
		if ((this.mode.equals("MULTI"))) {
			xPlayer = random(1, this.getxSize() - 2);
			yPlayer = random(1, this.getxSize() - 2);
			while (containsO(objects, new Player(xPlayer, yPlayer,1)) || ((xPlayer == p.getX() && yPlayer == p.getY()))) {
				xPlayer = random(1, this.getxSize() - 2);
				yPlayer = random(1, this.getxSize() - 2);
			}
			p2 = new Player(xPlayer, yPlayer,2);
		}
		this.LoadMap();
	}

	/**
	 * This method generates a random numbed within a given range
	 * 
	 * @pre the maximum limit > the minimum limit
	 * @param min - the minimum value of the random number
	 * @param max - the maximum value of the random number
	 * @post the returned number is within the specified range
	 * @return int - the random number
	 * 
	 */
	
	public int random(int min, int max) {
		Random rand = new Random();
		return rand.nextInt(max - min) + min;
	}

	/**
	 * This method adds a template to the map
	 * 
	 * @pre x and y have to be greater than 0 and f must be valid
	 * @param x - the width of the map
	 * @param y - the height of the map
	 * @param f - the file from which to read the map from
	 * @post map is added
	 * @return void
	 * 
	 */
	
	
	public void addMap(int x, int y, File f) {
		Scanner rowScanner = null;
		Scanner colScanner = null;
		String symbol = null;
		String line = null;
		try {
			rowScanner = new Scanner(new FileReader(f));
			int row = 0;
			int col = 0;
			while (rowScanner.hasNextLine()) {
				line = rowScanner.nextLine();
				colScanner = new Scanner(line);
				while (colScanner.hasNext()) {
					symbol = colScanner.next();
					switch (symbol) {
					case "1":
						replace(row + x, col + y, "EMPTY");
						break;
					case "2":
						replace(row + x, col + y, "WALL");
						break;
					case "3":
						replace(row + x, col + y, "GOAL");
						break;
					case "4":
						replace(row + x, col + y, "BLOCK");
						break;
					default:
					}
					row++;
				}
				col++;
				row = 0;
			}
		} catch (FileNotFoundException e) {

		}
	}

	/**
	 *  Replaces current map objects with given templates 
	 * 
	 * @pre existing map and existing templates
	 * @param 	x - location in x
	 * 			y - location in y
	 * 			object - template to replace
	 * @post places template into location (x,y) onto map
	 * @return void
	 * 
	 */
	
	public void replace(int x, int y, String object) {
		Iterator<Wall> itrW = Walls.iterator();
		while (itrW.hasNext()) {
			Wall curWall = itrW.next();
			if (curWall.getX() == x && curWall.getY() == y) {
				itrW.remove();
			}
		}
		switch (object) {
		case "EMPTY":
			break;
		case "WALL":
			Walls.add(new Wall(x, y));
			break;
		default:
		}
	}

	/**
	 * This method loads a level from a given input file which contains the
	 * an ascii representation of the game.
	 * 
	 * @pre file exists and contains valid ascii codes
	 * @param f - the input file to be read and converted
	 * @post a new level is loaded
	 * @return void
	 * 
	 */
	
	public void loadLevel(File f) {
		
		Scanner rowScanner = null;
		Scanner colScanner = null;
		String symbol = null;
		String line = null;
		try {
			rowScanner = new Scanner(new FileReader(f));
			int col = 0;
			int row = 0;
			while (rowScanner.hasNextLine()) {
				line = rowScanner.nextLine();
				colScanner = new Scanner(line);
				while (colScanner.hasNext()) {
					symbol = colScanner.next();
					switch (symbol) {
					case "2":
						Floors.add(new Floor(col, row));
						Walls.add(new Wall(col, row));
						break;
					case "1":
						Floors.add(new Floor(col, row));
						break;
					case "3":
						Floors.add(new Floor(col, row));
						Goals.add(new Goal(col, row));
						break;
					case "4":
						Floors.add(new Floor(col, row));
						Boxes.add(new Box(col, row));
						break;
					case "5":
						Floors.add(new Floor(col, row));
						p = new Player(col, row,1);
						break;
					case "6":
						Floors.add(new Floor(col, row));
						p2 = new Player(col, row,2);
						break;
					default:
						break;
					}
					col++;
				}
				row++;
				col = 0;
			}
			
			rowScanner.close();
			colScanner.close();
			
		} catch (FileNotFoundException e) {

		}

		this.LoadMap();
	}
	
	
	/**
	 * This method retrieves the dimensions of a map to be created from a file.
	 * 
	 * @pre file exists and z is either x or y
	 * @param z - this is x or y depending on whether the width or height is required
	 * @param f - the file to be read from which contains the ascii map
	 * @post the correct value is retrieved
	 * @return int - the height or the width of the map, depending on the input z 
	 * 
	 */
	
	private int getDimensions(String z, File f){
		Scanner rowScanner = null;
		Scanner colScanner = null;
		String line = null;
		try {
			rowScanner = new Scanner(new FileReader(f));
			int row = 0;
			int col = 0;
			int flag = 0;
			while (rowScanner.hasNextLine()) {
				line = rowScanner.nextLine();
				colScanner = new Scanner(line);
				while (colScanner.hasNext()) {
					colScanner.next();
					if(flag == 0){
						col++;
					}
				}
				row++;
				flag = 1;
			}
			
			rowScanner.close();
			colScanner.close();
			
			if(z.equals("x")){
				return col;
			} else {
				return row;
			}
			
		} catch (FileNotFoundException e) {
			return 0;
		}
	}
	
	/**
	 * This sets up the size of the map to be created
	 * 
	 * @pre x and y values are valid and greater than 0
	 * @param x - the width of the map
	 * @param y - the height of the map
	 * @post map size is initialised
	 * @return void
	 * 
	 */
	
	private void initialiser(int x, int y){
		
		this.xSize = x;
		this.ySize = y;
		
		if(x > y){
			this.scale = 800 / xSize;
			this.map = new int[xSize][xSize];
		} else {
			this.scale = 800 / ySize;
			this.map = new int[ySize][ySize];
		}
		
		
	}
	
	/**
	 * Check if object intersects with ArrayList of objects
	 * 
	 * @pre existing ArrayList
	 * @param 	ArrayList<SpriteProperties> - arraylist of objects
	 *			SpriteProperties - object to be checked
	 * @post returns boolean if objects intersects with any other objects in the ArrayList
	 * @return boolean
	 * 
	 */
	
	private boolean containsO(ArrayList<SpriteProperties> arrayList, SpriteProperties object) {
		for (SpriteProperties b : arrayList) {
			if ((b.getX() == object.getX()) && (b.getY() == object.getY())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Combines objects and return ArrayList
	 * 
	 * @pre existing ArrayList of objects e.g Goals,Walls,Floors etc
	 * @param n/a
	 * @post returns ArrayList of all objects on map
	 * @return ArrayList<SpriteProperties> 
	 * 
	 */
	
	public ArrayList<SpriteProperties> getAllObjects() {

		this.LoadMap();
		ArrayList<SpriteProperties> objects = new ArrayList<SpriteProperties>();

		for (Wall w : this.Walls) {
			if (w != null) {
				objects.add(w);
			}
		}

		for (Goal g : this.Goals) {
			if (g != null) {
				objects.add(g);
			}
		}

		for (Box b : this.Boxes) {
			if (b != null) {
				objects.add(b);
			}
		}
		return objects;
	}
	
	/**
	 * This method clears the map and gets ready for the new map to be created.
	 * 
	 * @pre map exists to be cleared
	 * @param n/a
	 * @post map is cleared
	 * @return void
	 * 
	 */
	
	public void clearMap() {
		map = new int[xSize][ySize];
		Floors.clear();
		Walls.clear();
		Goals.clear();
		Boxes.clear();
		LoadMap();
	}

	/**
	 * Retrieves the width of the map
	 * 
	 * @pre map to get size from is valid
	 * @param n/a
	 * @post true
	 * @return int - the width of the map
	 * 
	 */
	
	public int getxSize() {
		return xSize;
	}

	/**
	 * Retrieves the height of the map
	 * 
	 * @pre map to get size from is valid
	 * @param n/a
	 * @post true
	 * @return int - the height of the map
	 * 
	 */
	
	public int getySize() {
		return ySize;
	}

	/**
	 * Retrieves the array of the map
	 * 
	 * @pre map to get array from is valid
	 * @param n/a
	 * @post true
	 * @return int - the array of the map
	 * 
	 */
	
	public int[][] getMap() {
		return map;
	}

	/**
	 * Retrieves the Player
	 * 
	 * @pre game is running and player is valid
	 * @param n/a
	 * @post true
	 * @return Player - the player required
	 * 
	 */
	
	public Player getP() {
		return p;
	}
	
	/**
	 * Sets the Player
	 * 
	 * @pre game is running and player is valid
	 * @param n/a
	 * @post true
	 * @return Player - the first player
	 * 
	 */
	
	public void setP(Player p) {
		this.p = p;
	}
	
	/**
	 * Retrieves the Player
	 * 
	 * @pre game is running and player is valid
	 * @param n/a
	 * @post true
	 * @return Player - the second player
	 * 
	 */
	
	public Player getP2() {
		return p2;
	}
	
	/**
	 * Sets the Player
	 * 
	 * @pre game is running and player is valid
	 * @param n/a
	 * @post true
	 * @return Player - the player to be set
	 * 
	 */
	
	public void setP2(Player p2) {
		this.p2 = p2;
	}

}
