import java.awt.Image;
import java.util.LinkedList;

import javax.swing.ImageIcon;

/**
 * The Player class contains the information required for each of the players
 *
 */

public class Player extends SpriteProperties {

	private boolean hasMoved;
	private Vector direction = null;
	public int movecounter = 1;
	public int start = 0;
	private int playerAnimationCounter = 1;
	private int player;
	private LinkedList<Image> right = new LinkedList<Image>();
	private LinkedList<Image> left = new LinkedList<Image>();
	private LinkedList<Image> up = new LinkedList<Image>();
	private LinkedList<Image> down = new LinkedList<Image>();
	
	/**
	 * The Player object represents the players for the mazes
	 * 
	 * @pre maze is valid for the players to be placed on
	 * @param x - the x-coordinate of the player to be placed
	 * @param y - the y-coordinate of the player to be placed
	 * @post player is placed correctly
	 * @return n/a
	 * 
	 */
	
	public Player(int x, int y, int player) {
		super(x, y);
		this.player = player;
		this.hasMoved = false;
		// this.direction= new Vector(0,1);
		this.setIcon();
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/1.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/2.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/3.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/4.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/5.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/6.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/7.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/8.png")).getImage());
		this.right.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/9.png")).getImage());

		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/10.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/11.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/12.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/13.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/14.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/15.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/16.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/17.png")).getImage());
		this.left.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/18.png")).getImage());

		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/19.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/20.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/21.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/22.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/23.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/24.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/25.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/26.png")).getImage());
		this.up.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/27.png")).getImage());

		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/28.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/29.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/30.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/31.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/32.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/33.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/34.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/35.png")).getImage());
		this.down.add(new ImageIcon(this.getClass().getResource("/resources/Sprite/" + player + "/36.png")).getImage());

	}
	
	/**
	 * Sets up direction for the character
	 * 
	 * @pre character exists and direction exists
	 * @param direction - Vector of movement
	 * @post adds direction to character
	 * @return void
	 * 
	 */
	
	@Override
	public void move(Vector direction) {
		this.start++;
		super.move(direction);
		setDirection(direction);
	}

	/**
	 * Sets up initial graphic for character
	 * 
	 * @pre character exists and direction exists
	 * @param n/a
	 * @post sets intial graphic
	 * @return void
	 * 
	 */
	
	public void setIcon() {
		if (this.direction == null || this.direction.equals(new Vector(0, 1))) {
				super.spriteIcon("/resources/Sprite/" + player + "/28.png");
		} else if (this.direction.equals(new Vector(0, -1))) {
				super.spriteIcon("/resources/Sprite/" + player + "/19.png");

		} else if (this.direction.equals(new Vector(-1, 0))) {
				super.spriteIcon("/resources/Sprite/" + player + "/10.png");

		} else if (this.direction.equals(new Vector(1, 0))) {
				super.spriteIcon("/resources/Sprite/" + player + "/1.png");

		}

		System.gc();

	}

	/**
	 * Return graphic for the character for direction
	 * 
	 * @pre character exists and direction exists
	 * @param direction - direction of the player in string
	 * @post returns graphic of player in that direction
	 * @return Image
	 * 
	 */
	
	public Image getPlayerIcon(String direction) {
		int j = 1;
		Image temp = null;
		LinkedList<Image> directionlist = new LinkedList<Image>();
		if (direction.equals("right")) {
			directionlist = this.right;
		} else if (direction.equals("left")) {
			directionlist = this.left;
		} else if (direction.equals("up")) {
			directionlist = this.up;
		} else {
			directionlist = this.down;
		}

		for (Image i : directionlist) {
			if (j == this.playerAnimationCounter) {
				temp = i;
				break;
			}
			j++;
		}
		if (this.playerAnimationCounter < 9 && this.hasMoved)
			this.playerAnimationCounter++;
		else
			this.playerAnimationCounter = 1;

		if (this.Rcounter < 9)
			this.Rcounter++;
		if (this.Rcounter == 9)
			this.hasMoved = false;
		return temp;
	}
	
	/**
	 * Retrieves the move counter for the player
	 * 
	 * @pre player is valid and movecounter is between 1 and 9
	 * @param n/a
	 * @post true
	 * @return int - the movecounter
	 * 
	 */
	
	public int getMovecounter() {
		return movecounter;
	}

	/**
	 * Sets the move counter for the player
	 * 
	 * @pre player is valid and input is between 1 and 9
	 * @param movecounter - the value to be set for the movecounter
	 * @post movecounter is set to the new value
	 * @return void
	 * 
	 */
	
	public void setMovecounter(int movecounter) {
		this.movecounter = movecounter;
	}
	
	/**
	 * Checks to see if the desired player has been moved
	 * 
	 * @pre player is valid
	 * @param n/a
	 * @post correct boolean value is returned
	 * @return boolean - TRUE if moved, FALSE if otherwise
	 * 
	 */
	public boolean isHasMoved() {
		return hasMoved;
	}

	/**
	 * Sets whether the player has moved or not
	 * 
	 * @pre player is valid
	 * @param hasMoved - TRUE if the player has been moved, FALSE if otherwise
	 * @post new boolean value has been assigned
	 * @return void
	 * 
	 */
	public void setHasMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}
	
	/**
	 * Retrieves the direction requested to be moved in
	 * 
	 * @pre direction has been set and player is valid
	 * @param n/a
	 * @post direction is retried
	 * @return Vector - the direction to be moved in
	 */
	public Vector getDirection() {
		return direction;
	}
	
	/**
	 * Sets the direction requested to be moved in
	 * 
	 * @pre direction is valid and only involves 1 block, player is valid
	 * @param direction - the direction requested
	 * @post new direction is set
	 * @return void
	 * 
	 */
	public void setDirection(Vector direction) {
		this.direction = direction;

		setIcon();
	}

}
