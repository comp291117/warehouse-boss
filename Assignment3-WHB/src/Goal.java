import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Goal extends SpriteProperties {
	
	/**
	 * The Goal object represents the Goal for the mazes
	 * @param x - the x-coordinate of the Goal to be placed
	 * @param y - the y-coordinate of the Goal to be placed
	 * @post Goal is placed correctly
	 * @return n/a
	 * 
	 */
	
	public Goal(int x, int y) {
		super(x, y);
		super.spriteIcon("/resources/Sprite/goal.png");
	}

	/**
	 * Checks if box is ontop of Goal
	 * 
	 * @pre: Exists a box and goal
	 * @param: n/a
	 * @post: return if box is ontop of Goal
	 * @return: boolean
	 * 
	 */
	
	public boolean hasBox(Box b) {
		return this.isThere(b.getX(), b.getY());
	}
	
	/**
	 * Starts sound Thread
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post starts new thread
	 * @return n/a
	 * 
	 */
	
	public static synchronized void playSoundssharing() {
		new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.
			public void run() {
				try {
					Clip clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem
							.getAudioInputStream(this.getClass().getResourceAsStream("/resources/Sprite/sharing.wav"));
					clip.open(inputStream);
					clip.start();

				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}).start();
	}
	
	/**
	 * Starts sound Thread
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post starts new thread
	 * @return n/a
	 * 
	 */
	
	public static synchronized void playSoundyee() {
		new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.
			public void run() {
				try {
					Clip clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem
							.getAudioInputStream(this.getClass().getResourceAsStream("/resources/Sprite/yee.wav"));
					clip.open(inputStream);
					clip.start();

				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}).start();
	}
}
