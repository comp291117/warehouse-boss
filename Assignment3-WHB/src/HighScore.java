import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.awt.Font;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * The HighScore class allows the user to view previously made high scores.
 * 
 * It presents the top 3 high scores in ascending order as the lower the time/number of moves, the better.
 * It does not allow for duplicates.
 * 
 * @author Eunike
 *
 */

public class HighScore {

	public HighScore(){
		
		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);
		
		JLabel background = new JLabel();
		background.setLayout(new BorderLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/HIGHSCORE.gif")));
		background.setLayout(new BorderLayout());
		
		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(background);
		
		JButton exit = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/exit.png")));
		JButton mainMenu = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/menu.png")));
		
		exit.setOpaque(false);
		exit.setContentAreaFilled(false);
		exit.setBorderPainted(false);
		 
		mainMenu.setOpaque(false);
		mainMenu.setContentAreaFilled(false);
		mainMenu.setBorderPainted(false);
		
		GridBagConstraints c  = new GridBagConstraints();

		c.insets = new Insets(10, 10, 10, 10);

		int y= 1;
		
		JLabel title = new JLabel("NORMAL HIGHSCORES");
		title.setFont(new Font("American Typewriter", Font.BOLD, 30));
		c.gridx = 0;
		c.gridy = y++;
		menuPanel.add(title,c);
		
		try{
			Scanner sc = new Scanner(new FileReader("NORMALHighscore.txt"));
			String line = null;
			int place = 0;
			
			if (sc.hasNextLine()) {
				line = sc.nextLine();
			}
			
			while (sc.hasNextLine() && place < 3) {
				line = sc.nextLine();
				String[] split = line.split("\\W+");
				line = split[0] + " " + Integer.parseInt(split[1]);
				JLabel label = new JLabel(line);
				label.setFont(new Font("American Typewriter", Font.PLAIN, 14));
				c.gridx = 0;
				c.gridy = y++;
				menuPanel.add(label,c);
				place++;
			}
			
			sc.close();
		} catch (IOException ex1) {
			System.out.println("File not found");
		}
		      
		JLabel title2 = new JLabel("TIMED HIGHSCORES");
		title2.setFont(new Font("American Typewriter", Font.BOLD, 30));
		c.gridx = 0;
		c.gridy = y++;
		menuPanel.add(title2,c);
		
		try{
			Scanner sc = new Scanner(new FileReader("TIMEDHighscore.txt"));
			String line = null;
			int place = 0;
			
			if (sc.hasNextLine()) {
				line = sc.nextLine();
			}
			
			while (sc.hasNextLine() && place < 3) {
				line = sc.nextLine();
				String[] split = line.split("\\W+");
				line = split[0] + " " + Integer.parseInt(split[1]);
				JLabel label = new JLabel(line);
				label.setFont(new Font("American Typewriter", Font.PLAIN, 14));
				c.gridx = 0;
				c.gridy = y++;
				menuPanel.add(label,c);
				place++;
			}
			
			sc.close();
		} catch (IOException ex1) {
			System.out.println("File not found");
		}
		
		c.gridx = 0;
		c.gridy = y++;
		menuPanel.add(mainMenu,c);
		
		c.gridx = 0;
		c.gridy = y++;	
		menuPanel.add(exit,c);
		
		
		frame.add(menuPanel, BorderLayout.CENTER);
		
		
		mainMenu.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new Menu();
			}
			
		});
		
		exit.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
			
		});	
	}
}
