import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.TreeMap;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * 
 * 
 *
 */
public class GameState extends JFrame implements KeyListener {


	private JFrame pause = new JFrame();
	private JFrame instructions = new JFrame();

	private BufferedImage bf;
	private int pauseState;
	private int openWindow;

	private int timeRemaining = 30;
	private Timer countdownTimer;

	static GameBoard game;
	static int level;
	static int moves;
	static int timeCompleted = 0;
	static String gameMode;

	/**
	 * This is the
	 * 
	 * @pre valid GameState is input
	 * @param mode
	 *            - the mode of the game: TIMED/NORMAL/MULTIPLAYER/RANDOM
	 * @post correct game is run
	 * @return n/a
	 * 
	 */
	
	public GameState(String mode) {
		super("Warehouse Boss");
		setSize(800, 800);
		setLocationRelativeTo(null);
		setResizable(true);
		setVisible(true);
		setFocusable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.pauseState = 0;
		level = 1;
		gameMode = mode;
		runGame();

		this.addKeyListener(this);

		ActionListener tick = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (pauseState == 0) {
					if (--timeRemaining > 0) {

					} else {
						countdownTimer.stop();
						JOptionPane.showMessageDialog(null, "GAME OVER!");
						dispose();
						new GameOver();
					}
				}
			}
		};
		if (mode.equals("TIMED")) {
			countdownTimer = new Timer(1000, tick);
			countdownTimer.start();
		}
		update();

	}
	
	/**
	 * Displays window of Game
	 * 
	 * @pre there is a existing game object
	 * @param n/a
	 * @post setup and opens up play area
	 * @return void
	 * 
	 */
	
	public void runGame() {
		game = new GameBoard();
	}

	// detect key presses
	@Override
	
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if (game.moveChar(new Vector(1, 0), "P1")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			if (game.moveChar(new Vector(-1, 0), "P1")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_UP) {
			if (game.moveChar(new Vector(0, -1), "P1")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			if (game.moveChar(new Vector(0, 1), "P1")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_D) {
			if (game.moveChar(new Vector(1, 0), "P2")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_A) {
			if (game.moveChar(new Vector(-1, 0), "P2")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_W) {
			if (game.moveChar(new Vector(0, -1), "P2")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_S) {
			if (game.moveChar(new Vector(0, 1), "P2")) {
				GameState.playSoundstep();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_F5) {
			game.restart();
		} else if (e.getKeyCode() == KeyEvent.VK_F6 && (gameMode.equals("MULTI") || gameMode.equals("RANDOM"))) {
			game.generateNew();
		} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if (this.getPauseState() == 0) {
				this.setPauseState(1);
			} else {
				this.setPauseState(0);
			}
		}

		update();

	}

	/**
	 * This method prints out the matrix of a map and repaints the map when
	 * needed. It also detects whether the level has been completed.
	 * 
	 * @pre game is avaliable to be updated/repainted
	 * @param n/a
	 * @post game is updated/repainted
	 * @return void
	 * 
	 */
	
	public void update() {

		// Carter added, use for the animination move (9fps)
		game.LoadMap();
		repaint();

		// System.out.println(game.toString());

		// Checks if the level has been completed
		if (game.hasWon()) {
			timeCompleted += 30 - timeRemaining;
			if (level == 9 && ((gameMode.equals("TIMED") && timeRemaining != 0) || gameMode.equals("NORMAL"))) {
				String name = JOptionPane.showInputDialog(null, "Good job! What is your name?");
				if (name != null) {
					if (gameMode.equals("TIMED")) {
						saveScore(timeCompleted, name, GameState.gameMode);
					} else {
						saveScore(moves, name, GameState.gameMode);
					}
				}
				level = -1;
				dispose();
				new HighScore();
			} else {
				level++;
				timeRemaining += 30;
				runGame();
			}
		}

	}

	/**
	 * Sets whether the game is paused or unpaused.
	 * 
	 * @pre there is a game valid to be paused
	 * @param state
	 *            - 0 if the game is not paused, 1 if the game is paused
	 * @post the pause state of the game is changed
	 * @return void
	 * 
	 */
	
	public void setPauseState(int state) {
		this.pauseState = state;
		if (state == 1) {
			setTitle("Paused");
			setFocusable(false);
			pauseGame();
		} else {
			setTitle("Warehouse Boss");
			setFocusable(true);
			pause.dispose();
		}
	}
	
	/**
	 * Paints out all objects in the game
	 * 
	 * @pre there are objects to be printed
	 * @param graphic object g
	 * @post all images are parsed to bufferImage which is then painted
	 * @return void
	 * 
	 */
	@Override
	
	public void paint(Graphics g) {
		bf = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);

		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (game != null) {
			game.PaintMap(bf.getGraphics(), this);
			this.move();
		}
		// TODO Small bug: Title bar covers the graphics can someone find out
		// how to find the height of the title bar?
		g.drawImage(bf, 0, 0, null);
		System.gc();
	}

	/**
	 * Displays the Pause menu when the game is paused.
	 * 
	 * @pre there is a game to be paused
	 * @param n/a
	 * @post pause menu is displayed when the game is paused and is closed when
	 *       the game is unpaused
	 * @return void
	 * 
	 */
	
	public void pauseGame() {

		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);

		JLabel background = new JLabel();
		background.setLayout(new BorderLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/PAUSE.gif")));

		pause.setTitle("Pause Screen");
		pause.setSize(400, 400);
		pause.setLocationRelativeTo(null);
		pause.setResizable(false);
		pause.setVisible(true);
		pause.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pause.setContentPane(background);

		JButton instructionsB = new JButton(
				new ImageIcon(this.getClass().getResource("/resources/Panel/instructions.png")));
		instructionsB.setOpaque(false);
		instructionsB.setContentAreaFilled(false);
		instructionsB.setBorderPainted(false);

		JButton menu = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/menu.png")));
		menu.setOpaque(false);
		menu.setContentAreaFilled(false);
		menu.setBorderPainted(false);

		JButton resume = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/resume.png")));
		resume.setOpaque(false);
		resume.setContentAreaFilled(false);
		resume.setBorderPainted(false);

		JButton exit = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/exit.png")));
		exit.setOpaque(false);
		exit.setContentAreaFilled(false);
		exit.setBorderPainted(false);

		GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(10, 10, 10, 10);

		c.gridx = 0;
		c.gridy = 1;
		menuPanel.add(resume, c);

		c.gridx = 0;
		c.gridy = 2;
		menuPanel.add(instructionsB, c);

		c.gridx = 0;
		c.gridy = 3;
		menuPanel.add(menu, c);

		c.gridx = 0;
		c.gridy = 4;
		menuPanel.add(exit, c);

		pause.add(menuPanel, BorderLayout.CENTER);

		resume.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPauseState(0);
				instructions.dispose();
				openWindow = 0;
			}
		});

		pause.setFocusable(true);
		pause.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					setPauseState(0);
					openWindow = 0;
					instructions.dispose();
				}

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

		});

		instructionsB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (openWindow == 0) {
					openWindow = 1;
					instructions();
				} else {
					JOptionPane.showMessageDialog(null, "Instructions window already open!");
				}
			}

		});

		menu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				pause.dispose();
				instructions.dispose();
				new Menu();
			}

		});

		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}

		});

		pause.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				setPauseState(0);
			}
		});

	}

	/**
	 * Displays the Instructions when the game is paused
	 * 
	 * @pre game is paused
	 * @param n/a
	 * @post instructions page is opened only if there are no other instances of
	 *       it
	 * @return void
	 * 
	 */
	
	private void instructions() {

		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);

		JLabel background = new JLabel();
		background.setLayout(new BorderLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/INSTRUCTIONS.gif")));

		instructions.setSize(800, 800);
		instructions.setLocationRelativeTo(null);
		instructions.setResizable(false);
		instructions.setVisible(true);
		instructions.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		instructions.setContentPane(background);
		instructions.setFocusable(true);

		JButton menu = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/back.png")));
		menu.setOpaque(false);
		menu.setContentAreaFilled(false);
		menu.setBorderPainted(false);

		menuPanel.add(menu);

		instructions.add(menuPanel, BorderLayout.PAGE_END);

		instructions.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					openWindow = 0;
					instructions.dispose();
				}

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

		});

		instructions.addWindowListener((new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				openWindow = 0;
			}

		}));

		menu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				openWindow = 0;
				instructions.dispose();
			}

		});

	}

	/**
	 * This method saves the scores of the games completed in a textfile before
	 * sorting it in another textfile.
	 * 
	 * @pre game must be either "TIMED" or "NORMAL"
	 * @param number
	 *            - either the time required to complete the levels (if in TIMED
	 *            mode) or the number of moves taken to complete the levels (if
	 *            in NORMAL)
	 * @param name
	 *            - the name of the player taken from user input
	 * @param mode
	 *            - the mode fo the game, either NORMAL or TIMED
	 * @post the score is saved correctly
	 * @return void
	 * 
	 */
	
	private void saveScore(int number, String name, String mode) {

		String writer = mode + "Unsorted.txt";
		String highscore = mode + "Highscore.txt";

		try {
			BufferedWriter output = new BufferedWriter(new FileWriter(writer, true));
			output.newLine();
			output.append(name + " " + number);
			output.close();

		} catch (IOException ex1) {
			System.out.printf("ERROR writing score to file: %s\n", ex1);
		}

		try {

			Scanner sc = new Scanner(new FileReader(writer));
			TreeMap<Integer, String> scores = new TreeMap<Integer, String>();

			String line = null;
			if (sc.hasNextLine()) {
				line = sc.nextLine();
			}
			while (sc.hasNextLine()) {
				line = sc.nextLine();
				String[] playerScores = line.split("\\W+");
				scores.put(Integer.parseInt(playerScores[1]), playerScores[0]);
			}

			try {
				File file = new File(highscore);
				file.delete();
			} catch (Exception e) {

			}

			ArrayList<Integer> keys = new ArrayList<Integer>(scores.keySet());
			Collections.sort(keys);

			for (Integer score : scores.keySet()) {
				BufferedWriter output = new BufferedWriter(new FileWriter(highscore, true));
				output.newLine();
				output.append(scores.get(score) + " " + score);
				output.close();
			}
			sc.close();
		} catch (IOException ex1) {
			System.out.println("File not found");
		}

	}

	/**
	 * This retrieves whether the game is paused or not.
	 * 
	 * @pre game must be running
	 * @param n/a
	 * @post true
	 * @return int - 0 if the game is not paused and 1 if otherwise
	 * 
	 */
	
	public int getPauseState() {
		return this.pauseState;
	}

	// not used methods
	
	@Override
	public void keyTyped(KeyEvent e) {
		// not used

	}
	
	// not used methods
	
	@Override
	public void keyReleased(KeyEvent e) {
		// not used
	}

	/**
	 * This retrieves the time remaining for the game
	 * 
	 * @pre game must be in TIMED mode
	 * @param n/a
	 * @post true
	 * @return int - the time remaining for the level
	 * 
	 */
	
	public int getTimeRemaining() {
		return timeRemaining;
	}

	/**
	 * Starts update thread
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post starts new thread that updates graphic
	 * @return n/a
	 * 
	 */
	
	public void move() {
		Thread thread = new Thread() {
			@Override
			public void run() {
				repaint();
				// column+=10;
				return;
			}

		};

		if (!thread.isAlive()) {
			thread.start();
		}
	}
	
	/**
	 * Starts sound Thread
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post starts new thread
	 * @return n/a
	 * 
	 */
	
	public static synchronized void playSoundstep() {
		  new Thread(new Runnable() {
		  // The wrapper thread is unnecessary, unless it blocks on the
		  // Clip finishing; see comments.
		    public void run() {
		      try {
		        Clip clip = AudioSystem.getClip();
		       
		        AudioInputStream inputStream = AudioSystem.getAudioInputStream(
		          this.getClass().getResourceAsStream("/resources/Sprite/Step.wav"));
		        clip.open(inputStream);
		        clip.start();
		       
		      
		      } catch (Exception e) {
		        System.err.println(e.getMessage());
		      }
		    }
		  }).start();
	}
}
