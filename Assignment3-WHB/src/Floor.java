/**
 * The Floor class contains the information required for each of the floors
 *
 */
public class Floor extends SpriteProperties{

	/**
	 * The Floor object represents the floor for the mazes
	 * @param x - the x-coordinate of the floor to be placed
	 * @param y - the y-coordinate of the floor to be placed
	 * @post floor is placed correctly
	 * @return n/a
	 * 
	 */
	
	public Floor(int x, int y){
		super(x,y);
		super.spriteIcon("/resources/Sprite/floor.png");
	}
	
}
