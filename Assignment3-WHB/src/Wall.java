/**
 * The Wall class contains the information required for each of the walls
 *
 */

public class Wall extends SpriteProperties{
	
	/**
	 * The Wall object represents the walls for the mazes
	 * 
	 * @pre maze is valid for the walls to be placed on
	 * @param x - the x-coordinate of the wall to be placed
	 * @param y - the y-coordinate of the wall to be placed
	 * @post wall is placed correctly
	 * @return n/a
	 * 
	 */
	
	public Wall(int x, int y){
		super(x,y);
		super.spriteIcon("/resources/Sprite/wall.png");
	}
	
}
