import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * The Box class contains the information required for each of the boxes
 *
 */

public class Box extends SpriteProperties {
	
	private boolean onGoal;
	
	/**
	 * This Box object represents the boxes for the mazes
	 * 
	 * @pre maze is valid for the boxes to be placed on
	 * @param x - the x-coordinate of the box to be placed
	 * @param y - the y-coordinate of the box to be placed
	 * @post box is placed correctly
	 * @return n/a
	 * 
	 */
	
	public Box(int x, int y){
		super(x,y);
		this.onGoal = false;
		super.spriteIcon("/resources/Sprite/box.png");
	}
	
	/**
	 * This method checks to see if the box is placed on a
	 * goal on the map
	 * 
	 * @pre there are goals on the map and box is valid
	 * @param n/a
	 * @post correctly determines if box is on a goal
	 * @return boolean - TRUE if box is on top of a goal
	 * and FALSE if it's not
	 * 
	 */
	
	public boolean isOnGoal() {
		return onGoal;
	}

	/**
	 * This method changes the colour of the box to green if
	 * the box is placed on a goal.
	 * 
	 * @pre boxes and goals are valid and on the map
	 * @param onGoal - TRUE if the box is on goal and FALSE
	 * if not
	 * @post colour of the box is changed accordingly
	 * @return void
	 * 
	 */
	
	public void setOnGoal(boolean onGoal) {
		this.onGoal = onGoal;
		setColour();
		if (onGoal) {
			Goal.playSoundssharing();
			Goal.playSoundyee();
		}
	}

	/**
	 * This method determines the colour to be set 
	 * 
	 * @pre box is valid
	 * @param n/a
	 * @post the colour of the box is GREEN if on goal
	 * @return void
	 * 
	 */
	
	public void setColour(){
		if(onGoal){
			super.spriteIcon("/resources/Sprite/green_box.png");
		}else{
			super.spriteIcon("/resources/Sprite/box.png");
		}
	}
	
	/**
	 * Starts sound Thread
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post starts new thread
	 * @return n/a
	 * 
	 */
	
	public static synchronized void playSoundsbox() {
		new Thread(new Runnable() {
		// The wrapper thread is unnecessary, unless it blocks on the
		// Clip finishing; see comments.
		public void run() {
		try {
			Clip clip = AudioSystem.getClip();
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(
			this.getClass().getResourceAsStream("/resources/Sprite/box.wav"));
			clip.open(inputStream);
			   clip.start();
			       
			      
			} catch (Exception e) {
			   System.err.println(e.getMessage());
			}
		}
	}).start();
		}
}
