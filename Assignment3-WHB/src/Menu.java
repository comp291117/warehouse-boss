import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * The main class for our game.
 * 
 * @author Eunike
 *
 */
public class Menu {
	static boolean flag=true;
	
	public static void main(String[] args) {
		new Menu();
	}

	/**
	 * Method that display window and buttons for StartGame, Multiplayer etc
	 * 
	 * @pre valid Menu is input
	 * @param n/a
	 * @post opens Menu
	 * @return n/a
	 * 
	 */

	public Menu() {
		if (Menu.flag==true){
			Menu.playSoundpass();
			Menu.flag=false;
		}
		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);
		menuPanel.setVisible(true);
		JLabel background = new JLabel();
		background.setLayout(new FlowLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/MAIN.gif")));
		background.setLayout(new BorderLayout());
		background.setVisible(true);

		JButton start = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/singleplayer.png")));
		JButton multiplayer = new JButton(
				new ImageIcon(this.getClass().getResource("/resources/Panel/multiplayer.png")));
		JButton timed = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/timed.png")));
		JButton exit = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/exit.png")));
		JButton instruction = new JButton(
				new ImageIcon(this.getClass().getResource("/resources/Panel/instructions.png")));
		JButton highscore = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/scores.png")));

		start.setOpaque(false);
		start.setContentAreaFilled(false);
		start.setBorderPainted(false);

		multiplayer.setOpaque(false);
		multiplayer.setContentAreaFilled(false);
		multiplayer.setBorderPainted(false);

		timed.setOpaque(false);
		timed.setContentAreaFilled(false);
		timed.setBorderPainted(false);

		exit.setOpaque(false);
		exit.setContentAreaFilled(false);
		exit.setBorderPainted(false);

		instruction.setOpaque(false);
		instruction.setContentAreaFilled(false);
		instruction.setBorderPainted(false);

		highscore.setOpaque(false);
		highscore.setContentAreaFilled(false);
		highscore.setBorderPainted(false);

		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setContentPane(background);
		frame.pack();

		GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(10, 10, 10, 10);

		c.gridx = 0;
		c.gridy = 1;
		menuPanel.add(start, c);

		c.gridx = 0;
		c.gridy = 2;
		menuPanel.add(multiplayer, c);

		c.gridx = 0;
		c.gridy = 3;
		menuPanel.add(timed, c);

		c.gridx = 0;
		c.gridy = 4;
		menuPanel.add(instruction, c);

		c.gridx = 0;
		c.gridy = 5;
		menuPanel.add(highscore, c);

		c.gridx = 0;
		c.gridy = 6;
		menuPanel.add(exit, c);

		frame.add(menuPanel, BorderLayout.CENTER);

		start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				start();
			}

		});

		multiplayer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				new GameState("MULTI");
				
			}
			
		});

		timed.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				new GameState("TIMED");
			}

		});

		instruction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				instructions();
			}

		});

		highscore.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				new HighScore();
			}

		});

		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}

		});

	}

	/**
	 * Displays Instructions page
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post opens Instruction page with instruction
	 * @return n/a
	 * 
	 */

	private void instructions() {

		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);

		JLabel background = new JLabel();
		background.setLayout(new BorderLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/INSTRUCTIONS.gif")));

		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(background);

		JButton menu = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/back.png")));
		menu.setOpaque(false);
		menu.setContentAreaFilled(false);
		menu.setBorderPainted(false);

		menuPanel.add(menu);

		frame.add(menuPanel, BorderLayout.PAGE_END);

		menu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new Menu();

			}

		});

	}

	/**
	 * Displays StartGame page
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post opens Start page with Single player GameModes
	 * @return n/a
	 * 
	 */

	private void start() {
		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);

		JLabel background = new JLabel();
		background.setLayout(new BorderLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/MAIN.gif")));

		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(background);

		JButton normal = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/normal.png")));
		normal.setOpaque(false);
		normal.setContentAreaFilled(false);
		normal.setBorderPainted(false);
		
	
		
		JButton random = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/generated.png")));
		random.setOpaque(false);
		random.setContentAreaFilled(false);
		random.setBorderPainted(false);

		JButton back = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/back.png")));
		back.setOpaque(false);
		back.setContentAreaFilled(false);
		back.setBorderPainted(false);

		GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(10, 10, 10, 10);

		c.gridx = 0;
		c.gridy = 1;
		menuPanel.add(normal, c);

		c.gridx = 0;
		c.gridy = 2;
		menuPanel.add(random, c);

		c.gridx = 0;
		c.gridy = 3;
		menuPanel.add(back, c);

		frame.add(menuPanel, BorderLayout.CENTER);

		normal.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new GameState("NORMAL");

			}

		});

		random.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new GameState("RANDOM");

			}

		});

		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new Menu();

			}

		});
	}
	
	/**
	 * Starts sound Thread
	 * 
	 * @pre valid GameState is input
	 * @param n/a
	 * @post starts new thread
	 * @return n/a
	 * 
	 */
	
	public static synchronized void playSoundpass() {
		  new Thread(new Runnable() {
		  // The wrapper thread is unnecessary, unless it blocks on the
		  // Clip finishing; see comments.
		    public void run() {
		      try {
		        Clip clip = AudioSystem.getClip();
		        AudioInputStream inputStream = AudioSystem.getAudioInputStream(
		          this.getClass().getResourceAsStream("/resources/Sprite/Musicpass.wav"));
		        clip.open(inputStream);
		        clip.start();
		        clip.loop(Clip.LOOP_CONTINUOUSLY);
		      
		      } catch (Exception e) {
		        System.err.println(e.getMessage());
		      }
		    }
		  }).start();
	}
}
