import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * The GameOver consists of the GameOver screen presented once the player runs out of time in Timed Mode. 
 * From here, you are able to view highscores, start again, exit or just go to the main
 * menu
 * 
 */
public class GameOver {
	
	/**
	 * This object contains all the visuals regarding the GameOver screen. It has
	 * buttons which allow you to navigate to other classes/features of the game.
	 * 
	 * @pre user must be in timed mode for this to appear
	 * @param mode - the game mode of the game (should be
	 * @post visuals are displayed correctly
	 * @return n/a
	 * 
	 */
	
	public GameOver(){
		JPanel menuPanel = new JPanel(new GridBagLayout());
		menuPanel.setOpaque(false);
		
		JLabel background = new JLabel();
		background.setLayout(new BorderLayout());
		background.setIcon(new ImageIcon(this.getClass().getResource("/resources/Panel/GAMEOVER.gif")));
		background.setLayout(new BorderLayout());
		
		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(background);
		
		JButton newGame = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/playagain.png")));
		JButton highscore = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/scores.png")));
		JButton mainMenu = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/menu.png")));
		JButton exit = new JButton(new ImageIcon(this.getClass().getResource("/resources/Panel/exit.png")));
		

		 newGame.setOpaque(false);
		 newGame.setContentAreaFilled(false);
		 newGame.setBorderPainted(false);

		 highscore.setOpaque(false);
		 highscore.setContentAreaFilled(false);
		 highscore.setBorderPainted(false);

		 mainMenu.setOpaque(false);
		 mainMenu.setContentAreaFilled(false);
		 mainMenu.setBorderPainted(false);

		 exit.setOpaque(false);
		 exit.setContentAreaFilled(false);
		 exit.setBorderPainted(false);
		 
		
		GridBagConstraints c  = new GridBagConstraints();
	      
		c.insets = new Insets(10, 10, 10, 10);
				
		c.gridx = 0;
		c.gridy = 1;
		menuPanel.add(newGame,c);
		
		c.gridx = 0;
		c.gridy = 2;
		menuPanel.add(highscore,c);
		
		c.gridx = 0;
		c.gridy = 3;
		menuPanel.add(mainMenu,c);
		
		c.gridx = 0;
		c.gridy = 4;
		menuPanel.add(exit,c);
		
		frame.add(menuPanel, BorderLayout.CENTER);
		
		newGame.addActionListener(new ActionListener(){
	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();		
				new GameState("TIMED");
			}
		});
		
		mainMenu.addActionListener(new ActionListener(){
	
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new Menu();
			}
			
		});
		
		highscore.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new HighScore();
			}
			
		});
		
		exit.addActionListener(new ActionListener(){
	
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
			
		});	
	}
}
