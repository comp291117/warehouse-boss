import java.awt.Image;

import javax.swing.ImageIcon;

/**
 * The SpriteProperties class contains all the information required for each of the
 * different sprites placed onto the map.
 * It is extended by the Wall, Box, Player and Floor classes.
 *
 */

public class SpriteProperties {
		// x,y current positions
		protected int xpos;
		protected int ypos;
		// x,y initial positions
		protected int xinit;
		protected int yinit;
		//image icon
		protected Image spriteIcon;
		
		int Rcounter =0;
		int Lcounter=0;
		int Ucounter=0;
		int Dcounter=0;
	
		/**
		 * This contains the information regarding the positions of the different
		 * sprites, including the initial position.
		 * 
		 * @pre the object is valid
		 * @param x - the x-coordinate of the object
		 * @param y - the y-coordinate of the object
		 * @post the initial positions and current positions are recorded
		 * @return n/a
		 * 
		 */
		
		public SpriteProperties(int x, int y){
			this.xinit = x;
			this.xpos = this.xinit;
			this.yinit = y;
			this.ypos = this.yinit;
	}

		/**
		 * This method updates the x and y coordinates of the object after it has
		 * been moved
		 * 
		 * @pre object is valid and direction only involves moving by 1 block
		 * @param direction - the direction to be moved
		 * @post the positio of the moved object is updated
		 * @return void
		 */
		
		public void move(Vector direction){
			this.setX(this.getX()+ direction.getDx());
			this.setY(this.getY()+ direction.getDy());
		}
		
		/**
		 * This sets the icons for the sprite
		 * 
		 * @pre object the image is to be assigned to is valid
		 * @param url - the location of the image
		 * @post object image is assigned
		 * @return void
		 * 
		 */
		
		protected void spriteIcon(String url){
			this.spriteIcon = new ImageIcon(this.getClass().getResource(url)).getImage();
		}
		
		/**
		 * Retrieves the icon for the object
		 * 
		 * @pre object is valid
		 * @param n/a
		 * @post true
		 * @return Image - the image assigned to the object
		 */
		
		public Image getIcon(){
			return this.spriteIcon;
		}
		
		/**
		 * Retrieves the x-value of the object
		 * 
		 * @pre object is valid
		 * @param n/a
		 * @post true
		 * @return int - the x-coordinate of the object
		 * 
		 */
		
		public int getX(){
			return this.xpos;
		}
		
		/**
		 * Retrieves the y-value of the object
		 * 
		 * @pre object is valid
		 * @param n/a
		 * @post true
		 * @return int - the y-coordinate of the object
		 * 
		 */
		
		public int getY(){
			return this.ypos;
		}
		
		/**
		 * Sets the x-coordinate of the object
		 * 
		 * @pre object is valid
		 * @param x - the x-coordinate
		 * @post true
		 * @return void
		 * 
		 */
		
		public void setX(int x){
			this.xpos = x;
		}
		
		/**
		 * Sets the y-coordinate of the object
		 * 
		 * @pre object is valid
		 * @param y - the x-coordinate
		 * @post true
		 * @return void
		 * 
		 */
		
		public void setY(int y){
			this.ypos = y;
		}
		
		/**
		 * Retrieves the initial x-value of the object
		 * 
		 * @pre object is valid
		 * @param n/a
		 * @post true
		 * @return int - the initial x-coordinate of the object
		 * 
		 */
		
		public int getXinit(){
			return this.xinit;
		}
		
		/**
		 * Retrieves the initial y-value of the object
		 * 
		 * @pre object is valid
		 * @param n/a
		 * @post true
		 * @return int - the initial y-coordinate of the object
		 * 
		 */
		
		public int getYinit(){
			return this.yinit;
		}
		
		/**
		 * Checks to see whether a certain object is at a certain location
		 * 
		 * @pre object is valid and location is valid
		 * @param x - the x-coordinate in question
		 * @param y - the y-coordinate in question
		 * @post map is valid
		 * @return boolean - TRUE if object is there, FALSE if otherwise
		 * 
		 */
		
		public boolean isThere(int x, int y){
		if (this.getX() == x && this.getY() == y)
			return true;
		return false;
	}
}
