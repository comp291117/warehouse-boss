/**
 * The Vector class contains the information required for vectors which are
 * used to indicate direction
 *
 */

public class Vector {
	
	private int dx;
	private int dy;
	
	/**
	 * The Vector object represents the direction the player is to be moved in
	 * 
	 * @pre numbers are valid
	 * @param dx - the change in x-value
	 * @param dy - the change in y-value
	 * @post Vector is assigned with the correct values
	 * @return n/a 
	 * 
	 */
	
	public Vector(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	/**
	 * This method determines if two vectors are equal
	 * 
	 * @pre vectors are valid
	 * @param other - the vector to be compared to
	 * @post correct boolean value is returned
	 * @return boolean - TRUE if equal and FALSE if otherwise
	 * 
	 */
	
	public boolean equals(Vector other) {
		return (this.getDx() == other.getDx() && this.getDy() == other.getDy());
	}

	/**
	 * This returns the change in x value requested
	 * 
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return int - the value of the change in x requested
	 * 
	 */
	
	public int getDx() {
		return dx;
	}

	/**
	 * This sets the change in x value
	 * 
	 * @pre true
	 * @param dx - the change in x value
	 * @post true
	 * @return void
	 * 
	 */
	
	public void setDx(int dx) {
		this.dx = dx;
	}
	
	/**
	 * This returns the change in y value requested
	 * 
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return int - the value of the change in y requested
	 * 
	 */
	
	public int getDy() {
		return dy;
	}
	
	/**
	 * This sets the change in y value
	 * 
	 * @pre true
	 * @param dy - the change in y value
	 * @post true
	 * @return void
	 * 
	 */
	
	public void setDy(int dy) {
		this.dy = dy;
	}

}
